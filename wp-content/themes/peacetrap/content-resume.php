<a href="<?php echo get_permalink() ?>">
<div class="post rounded shadow m-3">
    <h1 class="post-title"><?php the_title(); ?></h1>
    <p class="post-info">
        Posté le <?php the_date(); ?> dans <?php the_category(', '); ?> par <?php the_author(); ?>.
    </p>
    <div class="post-content">
        <?php echo substr(get_the_content(),0,256).'...'; ?>
    </div>
</div>
</a>