<div class="post rounded shadow m-3">
    <h1 class="post-title"><?php the_title(); ?></h1>
    <p class="post-info">
        Posté le <?php the_date(); ?> dans <?php the_category(', '); ?> par <?php the_author(); ?>.
    </p>
    <div class="post-content">
        <?php the_content(); ?>
    </div>
</div>