
        </div>
    </main>
    <footer>
        <div class="container p-3">
            <div class="sidebar-bottom">
                <?php dynamic_sidebar( 'bottom' ); ?>
            </div>
            <?php wp_nav_menu([
                'menu' => 'bottom',
                'menu_class' => 'bottom-menu nav justify-content-center display-4',
                'before' => '<div class="nav-link">',
                'after' => '</div>',
            ]);
            wp_footer(); ?>
        </div>
    </footer>
</body>
</html>