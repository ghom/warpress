<?php

add_action('init', function(){
    register_nav_menus([
        'top' => 'top menu',
        'bottom' => 'bottom menu'
    ]);
});

add_action('widgets_init', function(){
    register_sidebar([
        'name' => 'sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ]);
    register_sidebar([
        'name' => 'bottom',
        'id' => 'bottom',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'
    ]);
    register_widget( 'Wardget' );
});

add_action('wp_enqueue_scripts', function(){
    wp_enqueue_script('jquery', get_template_directory_uri().'/bootstrap/jquery-3.4.1.min.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/bootstrap/bootstrap.bundle.js', array('jquery'), null, true);
    wp_enqueue_style('bootstrap', get_template_directory_uri().'/bootstrap/bootstrap.css');
    wp_enqueue_style('style', get_template_directory_uri().'/style.css');
});

class Wardget extends WP_Widget
{
    public function __construct(){
        parent::__construct('wardget','Wardget',[
            'description' => 'Gneuh'
        ]);
        $this->defaultClass = 'wardget';
        $this->defaultHTML = '👀 by <span> Ghom </span>';
        $this->defaultCSS = '.wardget { font-size: 35px; color: black; } .wardget span { color: blue; }';
    }

    public function widget($args,$instance){
        $class = isset($instance['class']) ? $instance['class'] : $this->defaultClass;
        $html = isset($instance['html']) ? $instance['html'] : $this->defaultHTML;
        $css = isset($instance['css']) ? $instance['css'] : $this->defaultCSS;

        echo $args['before_widget'];
        echo '<style type="text/css">'.$css.'</style>';
        echo '<div class="'.$class.'">'.$html.'</div>';
        echo $args['after_widget'];
    }

    public function form($instance){
        $class = isset($instance['class']) ? $instance['class'] : $this->defaultClass;
        $html = isset($instance['html']) ? $instance['html'] : $this->defaultHTML;
        $css = isset($instance['css']) ? $instance['css'] : $this->defaultCSS;

        ?><p style="white-spaces: nowrap; white-space: nowrap;">
            Classe : 
            <input 
                type="text" 
                class="widefat code" 
                name="<?php echo $this->get_field_name( 'class' ); ?>" 
                value="<?php echo esc_attr( $class ); ?>"
            /><br>
            HTML : 
            <input 
                type="text" 
                class="widefat code" 
                name="<?php echo $this->get_field_name( 'html' ); ?>" 
                value="<?php echo esc_attr( $html ); ?>"
            /><br>
            CSS : 
            <input 
                type="text" 
                class="widefat code" 
                name="<?php echo $this->get_field_name( 'css' ); ?>" 
                value="<?php echo esc_attr( $css ); ?>"
            />
        </p><?php
    }

    public function update($new_instance,$old_instance){
        $instance = [
            'class' => isset($new_instance['class']) ? $new_instance['class'] : $old_instance['class'],
            'html' => isset($new_instance['html']) ? $new_instance['html'] : $old_instance['html'],
            'css' => isset($new_instance['css']) ? $new_instance['css'] : $old_instance['css'],
        ];
        return $instance;
    }
}