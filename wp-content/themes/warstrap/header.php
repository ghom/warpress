<!DOCTYPE html>
<html lang="fr">
<head>
    <?php wp_head() ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name') ?></title>
</head>
<body class="min-vh-100 d-flex flex-column"> <!-- not work -->
    <header>
        <div class="container "> <!-- work -->
            <h1 class="display-1"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
            <?php wp_nav_menu([
                'menu' => 'top',
                'menu_class' => 'top-menu btn-group nav p-3',
                'before' => '<div class="shadow btn btn-light">',
                'after' => '</div>',
            ]); ?>
        </div>
    </header>
    <main> <!-- not work -->
        <div class="container bg-light rounded text-dark p-3 shadow">
            <div class="shadow-lg">
                <?php dynamic_sidebar( 'sidebar' ); ?>
            </div>

